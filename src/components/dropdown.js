import React, { Component } from "react";
import Transition from 'react-transition-group/Transition';
class Dropdown extends Component {
  state = {
    show: false,
    apps: [],
    selected: null
  };

  componentWillMount() {
    let allApps = [];
    this.props.user.apps.map((app, key) => (allApps[app.id] = app));
    this.setState({ apps: allApps });
  }

  selectApp(app) {
    this.setState({ selected: app, show: !this.state.show });
    this.props.onSelect.bind(this, app);
  }

  render() {
    const style = {
      withoutIcon: {
        paddingTop: "15px"
      },
      arrowWithImage: {
        marginTop: "-21px"
      },
      arrowWithoutImage: {
        marginTop: "-8px"
      }
    };
    const duration = 300;

    const defaultStyle = {
   opacity: 0,
   transition: 'all 0.3s ease-in-out',
    }

    const transitionStyles = {
      entering: { opacity:0},
      entered:  {opacity:1 }
    };
    return (
      <div className="App">
        <header className="App-header">
          <div>
            <div
              className="drop-head"
              onClick={() => {
                this.setState({ show: !this.state.show });
              }}
            >
              {this.state.selected ? (
                <h4>
                  {this.state.selected.iconUrl !== null ? (
                    <img alt="icon" src={this.state.selected.iconUrl} />
                  ) : (
                    <div
                      className="img-area"
                      style={{
                        marginRight: "10px",
                        height: "50px",
                        width: "50px"
                      }}
                    />
                  )}
                  {this.state.selected.name}
                </h4>
              ) : (
                <h4 style={style.withoutIcon}>Select App</h4>
              )}
              <span
                className="drpdn"
                style={
                  this.state.selected
                    ? style.arrowWithImage
                    : style.arrowWithoutImage
                }
              />
            </div>
            <Transition in={this.state.show} timeout={duration}>
                {(state) => (
                  <div style={{
                    ...defaultStyle,
                    ...transitionStyles[state]
                  }}>
                  <div className='animate'>

                  {this.props.user.organizations.map((organization, key) => (
                                    <div className="items-list" key={key}>
                                      <div className="orgnatzation-heading">
                                        <img src="./brifecase-16.png" alt="case" />
                                        <span>{organization.name}</span>
                                      </div>
                                      {organization.appIds.length > 0 ? (
                                        <ul className="app_main" key={key}>
                                          {organization.appIds.map((app, key) => {
                                            return (
                                              <li
                                                key={key}
                                                onClick={this.selectApp.bind(
                                                  this,
                                                  this.state.apps[app]
                                                )}
                                              style={{cursor:'pointer'}}
                                              >
                                                {this.state.apps[app].iconUrl !== null ? (
                                                  <img
                                                    alt="icon"
                                                    src={this.state.apps[app].iconUrl}
                                                  />
                                                ) : (
                                                  <div className="img-area" />
                                                )}
                                                <a> {this.state.apps[app].name} </a>
                                              </li>
                                            );
                                          })}
                                        </ul>
                                      ) : (
                                        <span style={{
                      color: '#606060',
                      fontWeight: 500,
                      padding: '4px 23px',
                      marginTop: '15px',
                      display: 'block'
                  }}>No apps in this Organization</span>
                                      )}
                                    </div>
                                  ))}
                  </div>
                  </div>
                )}
              </Transition>


          </div>
        </header>
      </div>
    );
  }
}

export default Dropdown;
