import React, { Component } from "react";
import Dropdown from "./components/dropdown";
const data = {
  user: {
    id: "9ae36100-1635-11e7-8b18-d11b7fbb6c81",
    email: "tigran@inapptics.com",
    fullName: "Tigran Hakobyan",
    avatar:
      "https://inapptics-console-avatars-e45pv.s3.amazonaws.com/9ae36100-1635-11e7-8b18-d11b7fbb6c81.jpg",
    lastTimezone: -180,
    isEmailVerified: true,
    lastSelectedApp: {
      appId: "demo-app-ios",
      buildType: 0
    },
    organizationIds: [
      "9af11ca0-1635-11e7-808a-8d9599ea3995",
      "fd201940-54e6-11e7-a7f7-9710cd42c21b"
    ],
    appIds: [
      "awesome-app-ios",
      "demo-app-ios",
      "gl-harut-ios",
      "gleisdorf-ashot-ios",
      "haruttestapp-ios",
      "hh-test-ios",
      "ionic-test-ios",
      "mtd-ios",
      "react-sample-ios",
      "simplaappbundletest-ios",
      "simple-app-v2-0-ios",
      "simpleapp-ashot-ios",
      "storyspot-ashot-ios",
      "storyspot-tigran-ios",
      "testtzapp-ios"
    ]
  },
  organizations: [
    {
      id: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      name: "Tigran Hakobyan",
      userRoleId: "owner_1490980514666",
      appIds: [
        "awesome-app-ios",
        "demo-app-ios",
        "gl-harut-ios",
        "gleisdorf-ashot-ios",
        "haruttestapp-ios",
        "hh-test-ios",
        "ionic-test-ios",
        "mtd-ios",
        "react-sample-ios",
        "simplaappbundletest-ios",
        "simple-app-v2-0-ios",
        "simpleapp-ashot-ios",
        "storyspot-ashot-ios",
        "storyspot-tigran-ios",
        "testtzapp-ios"
      ]
    },
    {
      id: "fd201940-54e6-11e7-a7f7-9710cd42c21b",
      name: "Tig",
      userRoleId: "org_member_1497873764277",
      appIds: []
    }
  ],
  apps: [
    {
      id: "awesome-app-ios",
      name: "Awesome app",
      apiKey: "wYvAfLiIha5MIOnL3tckB9POJ261dyoIa5bX6hdk",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl: null,
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1495533088020",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [],
        release: []
      }
    },
    {
      id: "demo-app-ios",
      name: "Demo App",
      apiKey: "lwoi8KGL6M4PZAREPEZSn8gDImlzlzof4B4nJ2Xe",
      bundleId: "am.socialobjects.simpleapp.demo",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl: "https://dx8wi30cklhff.cloudfront.net/demo-app-ios/latest.png",
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1497610907780",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [10236, 6573, 2345, 2036, 2035, 1789, 1111, 303],
        release: []
      }
    },
    {
      id: "gl-harut-ios",
      name: "gl-harut",
      apiKey: "ZLO45qRZR22emRobhladM8txELIQobFk3CBcowRi",
      bundleId: "at.muevo.gleisdorfapp.ashot",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl: "https://dx8wi30cklhff.cloudfront.net/gl-harut-ios/latest.png",
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: true
      },
      userRoleId: "app_admin_1495011480744",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [1467],
        release: []
      }
    },
    {
      id: "gleisdorf-ashot-ios",
      name: "Gleisdorf Ashot",
      apiKey: "0BAQ0ml2mm7WUEmBaswFzOB9jPARB0x5Bjj5yjM8",
      bundleId: "at.muevo.gleisdorfapp.test.ashot",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl:
        "https://dx8wi30cklhff.cloudfront.net/gleisdorf-ashot-ios/latest.png",
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1491926644345",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [1452],
        release: []
      }
    },
    {
      id: "haruttestapp-ios",
      name: "HarutTestApp",
      apiKey: "uCzQYq4X1i3Vh1pHgMFHv6gzLVSc7JIF1dEELxZI",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl: null,
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1494698975687",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [],
        release: []
      }
    },
    {
      id: "hh-test-ios",
      name: "hh test",
      apiKey: "0d7e9010d9ac11e7b75ff5efd04ce007",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl: null,
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1512471864286",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [],
        release: []
      }
    },
    {
      id: "ionic-test-ios",
      name: "Ionic test",
      apiKey: "T1W2xO2yqC2v7wUes5nZY8qRp6uYSZqI9dUMSwp1",
      bundleId: "io.ionic.conferenceapp",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl: "https://dx8wi30cklhff.cloudfront.net/ionic-test-ios/latest.png",
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1494695241971",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [2949],
        release: []
      }
    },
    {
      id: "mtd-ios",
      name: "MTD",
      apiKey: "EMLf6uN6jp4uk7Ez9P0XE1IXxQF7Lb9J9aiYG9Sj",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl: null,
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1498230525556",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [],
        release: []
      }
    },
    {
      id: "react-sample-ios",
      name: "react sample",
      apiKey: "LY22IPiNrK644zm03JQm53DrDRPTaBH54yeXsr7H",
      bundleId: "org.reactjs.native.example.AwesomeProject.inapptics",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl: null,
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1494714286982",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [1454],
        release: []
      }
    },
    {
      id: "simplaappbundletest-ios",
      name: "SimplaAppBundleTest",
      apiKey: "98ff1b60c62311e78f606f2ce56e0897",
      bundleId: "am.socialobjects.simpleapp.v1",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl: null,
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1510324185194",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [],
        release: []
      }
    },
    {
      id: "simple-app-v2-0-ios",
      name: "Simple App V2.0",
      apiKey: "1oPcnDAnDzaVgnOkh9e0778jp6sMlzdL3LTsaEwv",
      bundleId: "am.socialobjects.simpleapp.v2",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl:
        "https://dx8wi30cklhff.cloudfront.net/simple-app-v2-0-ios/latest.png",
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1498131698507",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [],
        release: []
      }
    },
    {
      id: "simpleapp-ashot-ios",
      name: "SimpleApp Ashot",
      apiKey: "RYwIPbPgwT0UA4XUd8nAaP3I5fhZSla4qwGAC2fc",
      bundleId: "am.socialobjects.simpleapp.v1",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl:
        "https://dx8wi30cklhff.cloudfront.net/simpleapp-ashot-ios/latest.png",
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.5,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1493903322010",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [
          6189,
          6152,
          2350,
          2346,
          1979,
          1978,
          1944,
          1879,
          1776,
          1770,
          1769,
          102
        ],
        release: []
      }
    },
    {
      id: "storyspot-ashot-ios",
      name: "Storyspot Ashot",
      apiKey: "IKfRa3TVHf5dzDc2ZXin43qi4gjVfAc5CTk7uPV1",
      bundleId: "se.storyspot.app.inapptics.ashot",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl:
        "https://dx8wi30cklhff.cloudfront.net/storyspot-ashot-ios/latest.png",
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: true,
        screenShotQuality: "0.8",
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1491927037264",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [1455],
        release: []
      }
    },
    {
      id: "storyspot-tigran-ios",
      name: "StorySpot Tigran",
      apiKey: "GasTvBN5YhaWFZiIvleq1eMcNBFGNtp3oOH3XPm7",
      bundleId: "se.storyspot.app.inapptics.arpy2",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl:
        "https://dx8wi30cklhff.cloudfront.net/storyspot-tigran-ios/latest.png",
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1491227574457",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [],
        release: []
      }
    },
    {
      id: "testtzapp-ios",
      name: "TestTzApp",
      apiKey: "a2d1f880d0e411e7a71c1b5a9997ced2",
      organizationId: "9af11ca0-1635-11e7-808a-8d9599ea3995",
      iconUrl: null,
      configs: {
        sdkEnabled: true,
        screenshotBlurLevel: 5,
        uploadOnWifiOnly: false,
        screenShotQuality: 0.8,
        doNotBlurDebugScreenshots: false
      },
      userRoleId: "app_admin_1511506606195",
      userSettings: {
        emailCrashReport: true,
        emailDailyStatsReports: true,
        emailMonthlyStatsReports: true,
        emailWeeklyStatsReports: true
      },
      appVersionIds: {
        debug: [],
        release: []
      }
    }
  ]
};

class App extends Component {

  getApp(app) {
    console.log(app)
  }
  render() {
    return (
      <div className="App">
        <Dropdown
          user={data}
          onSelect={this.getApp.bind(this)}
        />
      </div>
    );
  }
}

export default App;
